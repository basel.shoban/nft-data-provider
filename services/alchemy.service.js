import { Network, Alchemy } from "alchemy-sdk";
import BigNumber from 'bignumber.js';

export function init() {
  const settings = {
    apiKey: process.env.ALCHEMY_API_KEY,
    network: Network.ETH_MAINNET,
  };

  const alchemy = new Alchemy(settings);

  const getOwnersForContract = async (nftContract) => {
    const response = await alchemy.nft.getOwnersForContract(nftContract);

    return response.owners;
  }

  const getAddressBalance = async (accountAddress) => {
    const response = await alchemy.core.getBalance(accountAddress)

    const safeBalance = new BigNumber(response.toString());
    const weiToEth = 1e18;
    const balance = safeBalance.dividedBy(weiToEth).toString()

    return `${balance} ETH`;
  }

  return {
    getOwnersForContract,
    getAddressBalance
  }
}
