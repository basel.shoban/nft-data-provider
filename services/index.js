import * as nftService from './nft.service.js';
import * as alchemyService from './alchemy.service.js';

export function register() {
  const services = {};

  services.alchemyService = alchemyService.init(services);
  services.nftService = nftService.init(services);

  return services;
}
