export function init(services) {
  const getNftsCommonOwners = async (firstNft, secondNft) => {
    const firstNftOwners = await services.alchemyService.getOwnersForContract(firstNft);
    const secondNftOwners = await services.alchemyService.getOwnersForContract(secondNft);

    return getCommonOwners(firstNftOwners, secondNftOwners);
  }

  const getCommonOwners = (firstNftOwners, secondNftOwners) => {
    const commonOwners = [];
    const ownersMap = new Map(firstNftOwners.map(owner => [owner, 1]));

    secondNftOwners.forEach(owner => {
      if (ownersMap.has(owner)) {
        commonOwners.push(owner);
      }
    });

    return commonOwners;
  }

  const getNftsCommonOwnerBalance = async (firstNft, secondNft) => {
    const commonOwners = await getNftsCommonOwners(firstNft, secondNft);
    const luckyOwnerIndex = Math.floor(Math.random() * (commonOwners.length - 1))

    return services.alchemyService.getAddressBalance(commonOwners[luckyOwnerIndex])
  }

  return {
    getNftsCommonOwners,
    getNftsCommonOwnerBalance
  }
}
