# NFT Data Provider

## Description:
Small project to provide data around 2 NFTs:
- BAYC `0xBC4CA0EdA7647A8aB7C2061c2E118A18a936f13D`
- COOL `0x1A92f7381B9F03921564a437210bB9396471050C`

## Setup
- Copy `.env.example` file and name it `.env`
- Generate [Alchemy API key](https://dashboard.alchemy.com/signup/) and set it in `.env`
- Install dependencies
  ```bash
  npm install
  ```
- Run the project
  ```bash
  npm start
  ```