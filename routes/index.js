import {buildResponse} from '../utils/response.builder.js';

export function register(app, services) {
  const BAYC = '0xBC4CA0EdA7647A8aB7C2061c2E118A18a936f13D';
  const COOL = '0x1A92f7381B9F03921564a437210bB9396471050C';

  app.get('/api/owners', async (req, res) => {
    const firstNft = req.body.firstNft || BAYC;
    const secondNft = req.body.secondNft || COOL;

    const owners = await services.nftService.getNftsCommonOwners(
      firstNft,
      secondNft
    );

    const responseData = buildResponse({data: owners});
    res.json(responseData);
  });

  app.get('/api/owners/balance', async (req, res) => {
    const firstNft = req.body.firstNft || BAYC;
    const secondNft = req.body.secondNft || COOL;

    const balance = await services.nftService.getNftsCommonOwnerBalance(
      firstNft,
      secondNft
    );

    const responseData = buildResponse({balance});
    res.json(responseData);
  });
}
