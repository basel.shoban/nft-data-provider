export function buildResponse(data) {
  return {
    status: 'ok',
    ...data,
  }
}
