import express from 'express';
import helmet from 'helmet';
import * as routes from './routes/index.js';
import * as servicesLoader from './services/index.js';


export function bootstrap(){
  const app = express();

  app.use(helmet());
  app.use(express.json({limit: '50mb'}));
  app.use(express.urlencoded({
      limit:    '50mb',
      extended: false,
  }));
  
  const services = servicesLoader.register()
  routes.register(app, services);

  return app;
}
