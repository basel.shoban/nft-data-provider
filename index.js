import * as dotEnvLoader from 'dotenv';
import * as appFactory from './app.js';

dotEnvLoader.config();

const app = appFactory.bootstrap();

const port = process.env.APP_PORT || 3000;
app.listen(port , () => {
  console.log(`listening on port: ${port}`);
});
